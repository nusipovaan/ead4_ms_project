package kz.iitu.notify_service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class Consumer {
    @KafkaListener(topics = "comment_requests", groupId = "group_id")
    public void consume(CommentRequest commentRequest) throws IOException {
//        System.out.println(String.format("#### -> Notify user by email: -> %s",
//                "User " + commentRequest.getUserId() + " purchased book "
//                        + commentRequest.getBook().toString()));
    }
}
