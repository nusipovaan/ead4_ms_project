package kz.iitu.quicklinks.service;

import kz.iitu.quicklinks.model.Recipe;

public interface RecipeService {
    public Recipe getRecipeById(Long recipeId);
}
