package kz.iitu.quicklinks.service.impl;

import kz.iitu.quicklinks.model.Quicklinks;
import kz.iitu.quicklinks.repository.QuicklinksRepository;
import kz.iitu.quicklinks.service.QuicklinksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuicklinksServiceImpl implements QuicklinksService {
    @Autowired
    private QuicklinksRepository quickRepo;



    @Override
    public void addRecipeToQuicklinks(Quicklinks q) {
        quickRepo.saveAndFlush(q);
    }

    @Override
    public void deleteQuicklink(Long id) {
        quickRepo.deleteById(id);
    }

    @Override
    public List<Quicklinks> getUsersQuicklinks(Long userId) {
        return quickRepo.findByUserId(userId);
    }
}
