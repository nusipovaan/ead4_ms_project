package kz.iitu.quicklinks.service;

import kz.iitu.quicklinks.model.Quicklinks;

import java.util.List;

public interface QuicklinksService {
    void addRecipeToQuicklinks(Quicklinks q);
    void deleteQuicklink(Long id);
    List<Quicklinks> getUsersQuicklinks(Long userId);
}
