package kz.iitu.quicklinks.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kz.iitu.quicklinks.model.Quicklinks;
import kz.iitu.quicklinks.service.QuicklinksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/quicklinks")
@Api(value = "Quicklinks controller", description = "Controller allows to  work with quicklinks")
public class QuicklinksController {

    @Autowired
    private QuicklinksService quicklinksService;

    @ApiOperation(value = "To get all recipes added in quicklinks of specific user", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/user/{user_id}")
    public ResponseEntity<?> getAllQuicklinks(@PathVariable Long user_id) {
        return ResponseEntity.ok(quicklinksService.getUsersQuicklinks(user_id));
    }

    @ApiOperation(value = "To delete specific quicklink by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteQuicklink(@PathVariable Long id) {
        quicklinksService.deleteQuicklink(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @ApiOperation(value = "To add recipe to quicklinks", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PostMapping("")
    public ResponseEntity<?> addComment(@RequestBody Quicklinks q) {
        quicklinksService.addRecipeToQuicklinks(q);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
}

