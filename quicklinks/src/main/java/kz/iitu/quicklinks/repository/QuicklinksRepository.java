package kz.iitu.quicklinks.repository;

import kz.iitu.quicklinks.model.Quicklinks;
import kz.iitu.quicklinks.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuicklinksRepository extends JpaRepository<Quicklinks, Long> {
    List<Quicklinks> findByUserId(Long userId);
}
