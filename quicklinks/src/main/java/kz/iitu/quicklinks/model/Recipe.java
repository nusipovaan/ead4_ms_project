package kz.iitu.quicklinks.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@NoArgsConstructor
public class Recipe {

    private Long id;
    private String name;
    private String description;
    private Long mealId;
    private Long authorId;
}
