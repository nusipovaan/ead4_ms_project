package com.iitu.critique.service;

import com.iitu.critique.model.Critique;
import com.iitu.critique.repository.CritiqueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CritiqueServiceImpl implements CritiqueService {

    @Autowired
    private CritiqueRepository critiqueRepository;


    @Override
    public List<Critique> getAllCritiques() {
        return critiqueRepository.findAll();
    }

    @Override
    public List<Critique> getUserCritiques(Long recipeId) {
        return critiqueRepository.findByRecipeId(recipeId);
    }

    @Override
    public List<Critique> getAuthorComments(Long authorId) {
        return critiqueRepository.findByAuthorId(authorId);
    }

    @Override
    public Critique getCritiqueById(Long id) {
        return critiqueRepository.findById(id).get();
    }

    @Override
    public void addCritique(Critique critique) {
        critiqueRepository.save(critique);
    }

    @Override
    public void deleteCritique(Long id) {
        critiqueRepository.deleteById(id);
    }

    @Override
    public void updateCritique(Critique critique) {
        Optional<Critique> commentOptional = critiqueRepository.findById(critique.getId());

        if (commentOptional.isPresent()) {
            Critique c = commentOptional.get();
            c.setText(critique.getText());
            critiqueRepository.saveAndFlush(c);
        }
    }
}
