package com.iitu.critique.service;

import com.iitu.critique.model.Critique;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {
    private static final String TOPIC = "comment_requests";

    @Autowired
    private KafkaTemplate<String, Critique> kafkaTemplate;

    public String critiqueRequestNotify(Critique critique) {
        System.out.println(String.format("#### -> Producing comment request to notification service -> %s", critique));
        this.kafkaTemplate.send(TOPIC, critique);
        return "Successfully";
    }
}
