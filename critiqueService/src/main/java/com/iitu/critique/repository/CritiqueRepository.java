package com.iitu.critique.repository;

import com.iitu.critique.model.Critique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CritiqueRepository extends JpaRepository<Critique, Long> {
    List<Critique> findByRecipeId(Long recipeId);
    List<Critique> findByAuthorId(Long authorId);

}
