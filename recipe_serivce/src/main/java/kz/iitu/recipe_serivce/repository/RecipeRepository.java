package kz.iitu.recipe_serivce.repository;

import kz.iitu.recipe_serivce.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    List<Recipe> findByMealId(Long mealId);
    List<Recipe> findByAuthorId(Long authorId);

}
