package kz.iitu.recipe_serivce.model;

import lombok.Data;

@Data
public class Comment {
    private Long id;

    private String text;
    private Long recipeId;
    private Long authorId;
}
