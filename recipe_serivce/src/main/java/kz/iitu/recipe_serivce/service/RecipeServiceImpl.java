package kz.iitu.recipe_serivce.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.recipe_serivce.model.Comment;
import kz.iitu.recipe_serivce.model.Recipe;
import kz.iitu.recipe_serivce.repository.RecipeRepository;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Recipe> getAllRecipes() {
        return recipeRepository.findAll();
    }

    @Override
    public List<Recipe> getMealRecipes(Long mealId) {
        return recipeRepository.findByMealId(mealId);
    }

    @Override
    public List<Recipe> getAuthorRecipes(Long authorId) {
        return recipeRepository.findByAuthorId(authorId);
    }

    @Override
    public Recipe getRecipeById(Long id) {
        return recipeRepository.findById(id).get();
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getAllCommentsOfRecipeFallback",
            threadPoolKey = "getAllCommentsOfRecipe",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
                    @HystrixProperty(name="maximumSize", value="120"),
                    @HystrixProperty(name="allowMaximumSizeToDivergeFromCoreSize", value="true"),
            })
    public List<Comment> getAllCommentsOfRecipe(Long id) {
//        List<Comment> commentList  = restTemplate.getForObject("http://comment-service/comments/recipe/"+ id, List.class);
//        return commentList;

        String apiCredentials = "rest-client:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange("http://comment-service/comments/recipe/"+ id,HttpMethod.GET, entity,List.class).getBody();
    }
    public List<Comment> getAllCommentsOfRecipeFallback(Long id) {
        List<Comment> commentList = new ArrayList<>();
        Comment comment = new Comment();
        comment.setId(0L);
        comment.setText("Text is not available: Service Unavailable");
        commentList.add(comment);
        return commentList;
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getRecipeRatingFallback",
            threadPoolKey = "getRecipeRating",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
            })
    public double getRecipeRating(Long id) {
        double rating =  restTemplate.getForObject("http://rating-service/rating/recipe/"+ id, Double.class);
        return rating;
    }
    public double getRecipeRatingFallback(Long id) {

        return 0;
    }

    @Override
    public void addRecipe(Recipe meal) {
        recipeRepository.save(meal);
    }

    @Override
    public void deleteRecipe(Long id) {
        recipeRepository.deleteById(id);
    }

    @Override
    public void updateRecipe(Recipe recipe) {
        Optional<Recipe> recipeOptional = recipeRepository.findById(recipe.getId());

        if (recipeOptional.isPresent()) {
            Recipe r = recipeOptional.get();
            r.setName(recipe.getName());
            recipeRepository.saveAndFlush(r);
        }
    }
}
