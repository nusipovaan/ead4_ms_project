package kz.iitu.recipe_serivce.service;

import kz.iitu.recipe_serivce.model.Comment;
import kz.iitu.recipe_serivce.model.Recipe;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RecipeService {
    List<Recipe> getAllRecipes();
    List<Recipe> getMealRecipes(Long mealId);
    List<Recipe> getAuthorRecipes(Long authorId);
    Recipe getRecipeById(Long id);
    List<Comment> getAllCommentsOfRecipe(Long id);
    double getRecipeRating(Long id);

    void addRecipe(Recipe meal);
    void deleteRecipe(Long id);
    void updateRecipe(Recipe meal);
}
