package kz.iitu.mealService.service;

import kz.iitu.mealService.model.Meal;
import kz.iitu.mealService.model.Recipe;

import java.util.List;

public interface MealService {
    List<Meal> findAllMeals();
    List<Recipe> getAllRecipesOfMeal(Long id);
    List<Meal> getCategoryMeals(Long categoryId);
    Meal getMealById(Long id);

    void addMeal(Meal meal);
    void deleteMeal(Long id);
    void updateMeal(Meal meal);
}
