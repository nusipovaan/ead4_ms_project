package kz.iitu.mealService.service;

import kz.iitu.mealService.model.Meal;
import kz.iitu.mealService.model.Recipe;
import kz.iitu.mealService.repository.MealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MealServiceImpl implements MealService {
    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Meal> findAllMeals() {
        return mealRepository.findAll();
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getAllRecipesOfMealFallback",
            threadPoolKey = "getAllRecipesOfMeal",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maxQueueSize", value="50")
            })
    public List<Recipe> getAllRecipesOfMeal(Long id) {
//        List<Recipe> recipeList  = restTemplate.getForObject("http://recipe-service/recipes/meal/"+ id, List.class);
//        return recipeList;
        String apiCredentials = "rest-client:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange("http://recipe-service/recipes/meal/"+ id,HttpMethod.GET, entity,List.class).getBody();
    }
    public List<Recipe> getAllRecipesOfMealFallback(Long id) {
        List<Recipe> rList = new ArrayList<>();
        rList.add(new Recipe(1L,"Recipe is not available: Service Unavailable",1L,"-"));
        return rList;
    }

    @Override
    public List<Meal> getCategoryMeals(Long categoryId) {
        return mealRepository.findByCategoryId(categoryId);
    }

    @Override
    public Meal getMealById(Long id) {
        return mealRepository.findById(id).get();
    }

    @Override
    public void addMeal(Meal meal) {
        mealRepository.save(meal);
    }

    @Override
    public void deleteMeal(Long id) {
        mealRepository.deleteById(id);
    }

    @Override
    public void updateMeal(Meal meal) {
        Optional<Meal> mealOptional = mealRepository.findById(meal.getId());

        if (mealOptional.isPresent()) {
            Meal m = mealOptional.get();
            m.setName(meal.getName());
            mealRepository.saveAndFlush(m);
        }
    }
}
