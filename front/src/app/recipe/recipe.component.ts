import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {
  id: any;
  recipes = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private catService: CategoryService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    // console.log(this.route.snapshot.routeConfig.path);
    this.catService.getRecipesofMeal(this.id).subscribe((res) => {
      this.recipes = res;
      // let mid = this.recipes['mealId']
      // this.catService.getMeal(this.recipes['mealId']).subscribe(r => {

      // })
    });
  }
  goToRecipe(id: any){
    this.router.navigate(['recipe/',id]);
  }

}
