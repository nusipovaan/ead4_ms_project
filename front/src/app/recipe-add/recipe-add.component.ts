import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-add',
  templateUrl: './recipe-add.component.html',
  styleUrls: ['./recipe-add.component.scss']
})



export class RecipeAddComponent implements OnInit {

  categories: Category[];
  meals: Meal[];

    selectedCategory: Category = {name: '',id: 0};
    selectedMeal: Meal = {categoryID: 0, name: '',id: 0};

    constructor() {
        this.categories = [
            {name: 'Breakfast', id: 1},
            {name: 'Lunch', id: 2},
            {name: 'Drinks', id: 3},
            {name: 'Desserts', id: 4}
        ];
        this.meals = [
            {categoryID: 4, name: 'Daily Girl', id: 1},
            {categoryID: 4, name: 'Red Velvet', id: 2},
            {categoryID: 1, name: 'Omelette', id: 3},
            {categoryID: 3, name: 'Mocco', id: 4},
            {categoryID: 3, name: 'Sandwich', id: 5}
        ];
    }

  ngOnInit(): void {
  }

}

interface Category {
  name: string,
  id: number
}

interface Meal {
  categoryID: number,
  name: string,
  id: number
}