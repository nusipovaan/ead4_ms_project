import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../services/category.service';
@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.scss'],
})
export class MealComponent implements OnInit {
  id: any;
  meals = [];
  constructor(
    private route: ActivatedRoute,
    private catService: CategoryService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    // console.log(this.route.snapshot.routeConfig.path);
    this.catService.getMeals(this.id).subscribe((res) => {
      this.meals = res;
    });
  }
}
