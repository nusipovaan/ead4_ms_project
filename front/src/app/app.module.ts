import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {AccordionModule} from 'primeng/accordion';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import { HttpClientModule } from '@angular/common/http';
import {DropdownModule} from 'primeng/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { CategoryComponent } from './category/category.component';
import { MealComponent } from './meal/meal.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipePageComponent } from './recipe-page/recipe-page.component';
import { AccordionComponent } from './accordion/accordion.component';
import { CommentComponent } from './comment/comment.component';
import { UserPageComponent } from './user-page/user-page.component';
import { RecipeAddComponent } from './recipe-add/recipe-add.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import {MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import { CritiqueAddComponent } from './critique-add/critique-add.component';
import { CritiqueComponent } from './critique/critique.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    CategoryComponent,
    MealComponent,
    RecipeComponent,
    RecipePageComponent,
    AccordionComponent,
    CommentComponent,
    UserPageComponent,
    RecipeAddComponent,
    CritiqueAddComponent,
    CritiqueComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CardModule,
    ButtonModule,
    AccordionModule,
    DialogModule,
    HttpClientModule,
    InputTextModule,
    DropdownModule,
    FormsModule,
    BrowserAnimationsModule,
    InputTextareaModule,
    ToastModule,
  ],
  providers: [
    AuthService,
    HttpClientModule,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
