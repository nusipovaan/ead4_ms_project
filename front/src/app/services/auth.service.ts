import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalVariable } from '../global';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public currentUser!: Observable<any>;
  private currentUserSubject!: BehaviorSubject<any>;
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(
      JSON.parse(JSON.stringify(localStorage.getItem('currentUser')))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }
  login(user: Object): Observable<any> {
    return this.http.post<any>(`${GlobalVariable.BASE_API}/auth`, user).pipe(
      map((u: any) => {
        console.log('UU', u);
        if (u) {
          localStorage.setItem('currentUser', JSON.stringify(u));
          localStorage.setItem('currentUserName', JSON.stringify(user));
          this.currentUserSubject.next(u);
          return u;
        }
      })
    );
  }
  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentUserName');
    window.location.reload();
    // this.currentUserSubject.next(null);
  }
}
