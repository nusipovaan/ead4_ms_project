package kz.iitu.authservice.common;

public enum UserRole {
    USER, ADMIN;
}
