package com.iitu.comment.service;

import com.iitu.comment.model.Comment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {
    List<Comment> getAllComments();
    List<Comment> getRecipeComments(Long recipeId);
    List<Comment> getAuthorComments(Long authorId);
    Comment getCommentById(Long id);

    void addComment(Comment comment);
    void deleteComment(Long id);
    void updateComment(Comment comment);
}
