package com.iitu.comment.service;

import com.iitu.comment.model.Comment;
import com.iitu.comment.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentRepository commentRepository;


    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public List<Comment> getRecipeComments(Long recipeId) {
        return commentRepository.findByRecipeId(recipeId);
    }

    @Override
    public List<Comment> getAuthorComments(Long authorId) {
        return commentRepository.findByAuthorId(authorId);
    }

    @Override
    public Comment getCommentById(Long id) {
        return commentRepository.findById(id).get();
    }

    @Override
    public void addComment(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }

    @Override
    public void updateComment(Comment comment) {
        Optional<Comment> commentOptional = commentRepository.findById(comment.getId());

        if (commentOptional.isPresent()) {
            Comment c = commentOptional.get();
            c.setText(comment.getText());
            commentRepository.saveAndFlush(c);
        }
    }
}
