package com.iitu.comment.service;

import com.iitu.comment.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {
    private static final String TOPIC = "comment_requests";

    @Autowired
    private KafkaTemplate<String, Comment> kafkaTemplate;

    public String commentRequestNotify(Comment comment) {
        System.out.println(String.format("#### -> Producing comment request to notification service -> %s", comment));
        this.kafkaTemplate.send(TOPIC, comment);
        return "Successfully";
    }
}
