package com.example.user_service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserServiceApplicationTests {

	@Test
	void contextLoads() {
	}

    @Test
    void isAccountNonExpired() {
    }

    @Test
    void isAccountNonLocked() {
    }

    @Test
    void isCredentialsNonExpired() {
    }

    @Test
    void isEnabled() {
    }

    @Test
    void getAuthorities() {
    }

    @Test
    void testToString() {
    }

    @Test
    void getId() {
    }

    @Test
    void getUsername() {
    }

    @Test
    void getPassword() {
    }

    @Test
    void getRoles() {
    }

    @Test
    void setId() {
    }

    @Test
    void setUsername() {
    }

    @Test
    void setPassword() {
    }

    @Test
    void setRoles() {
    }
}
