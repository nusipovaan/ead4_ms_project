package com.example.user_service.service;

import com.example.user_service.model.Recipe;
import com.example.user_service.model.User;

import java.util.List;

public interface UserService {
    public List<User> findAllUsers();

    public User findUserById(Long id);
    List<Recipe> findAllUserRecipes(Long id);
    List<Recipe> findAllUserQuicklinks(Long id);
     void createUser(User user);
    void createRecipe(Recipe recipe);

    public void deleteUser(Long id);

    public void updateUser(User user);
}