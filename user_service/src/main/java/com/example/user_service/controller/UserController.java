package com.example.user_service.controller;

import com.example.user_service.model.Recipe;
import com.example.user_service.model.User;
import com.example.user_service.service.Producer;
import com.example.user_service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@Api(value = "User controller", description = "Controller allows to interact with user data")
public class UserController {

    private final Producer producer;

    @Autowired
    private UserService userService;

    public UserController(Producer producer, UserService userService) {
        this.producer = producer;
        this.userService = userService;
    }

    @ApiOperation(value = "To get all users from the database", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("")
    public List<User> findAllUsers() {
        return userService.findAllUsers();
    }

    @ApiOperation(value = "To get a data of specific user by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/{id}")
    public User findUserById(@PathVariable Long id) {
        return userService.findUserById(id);
    }

    @ApiOperation(value = "To get all recipes which was created by user", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/{id}/recipes")
    public List<Recipe> findUserRecipes(@PathVariable Long id) {
        return userService.findAllUserRecipes(id);
    }

    @ApiOperation(value = "To get all recipes which was added to quicklinks by user", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/{id}/quicklinks")
    public List<Recipe> findUserQuicklinks(@PathVariable Long id) {
        return userService.findAllUserQuicklinks(id);
    }

    @ApiOperation(value = "To add a new user", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PostMapping("")
    public void addUser(@RequestBody User user) {
        userService.createUser(user);
    }

    @ApiOperation(value = "To delete user by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @ApiOperation(value = "To update user by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PutMapping("/{id}")
    public void updateUser(@PathVariable Long id,@RequestBody User user) {
        user.setId(id);
        userService.updateUser(user);
    }
//
//    @GetMapping("/me")
//    public User findUserById() {
//        User user = (User) userService.loadUserByUsername(username);
//        return user;
//    }

}