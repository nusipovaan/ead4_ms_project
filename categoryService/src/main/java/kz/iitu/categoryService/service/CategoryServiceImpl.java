package kz.iitu.categoryService.service;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import kz.iitu.categoryService.model.Category;
import kz.iitu.categoryService.model.Meal;
import kz.iitu.categoryService.repository.CategoryRepository;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<Category> findAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getAllMealsOfCategoryFallback",
            threadPoolKey = "getAllMealsOfCategory",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maxQueueSize", value="50"),

            })
    public List<Meal> getAllMealsOfCategory(Long id) {
//        List<Meal> mealList  = restTemplate.getForObject("http://meal-service/meals/category/"+ id, List.class);
//        return mealList;
//        String apiCredentials = "rest-client:p@ssword";
//        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
//        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);
//
        return restTemplate.exchange("http://meal-service/meals/category/"+ id, HttpMethod.GET, entity,List.class).getBody();
    }
    public List<Meal> getAllMealsOfCategoryFallback(Long id) {
        List<Meal> mealList = new ArrayList<>();
        mealList.add(new Meal(1L,"Meals is not available: Service Unavailable",1L,"-"));
        return mealList;
    }

    @Override
    public Category getMealCategoryById(Long id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public void addCategory(Category category) {
         categoryRepository.save(category);
    }

    @Override
    public void deleteCategory(Long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public void updateCategory(Category category) {
        Optional<Category> categoryOptional = categoryRepository.findById(category.getId());

        if (categoryOptional.isPresent()) {
            Category c = categoryOptional.get();
            c.setName(category.getName());
            categoryRepository.saveAndFlush(c);
        }
    }
}
