package kz.iitu.categoryService.service;

import kz.iitu.categoryService.model.Category;
import kz.iitu.categoryService.model.Meal;

import java.util.List;

public interface CategoryService {

     List<Category> findAllCategories();
     List<Meal> getAllMealsOfCategory(Long id);
     Category getMealCategoryById(Long id);


     void addCategory(Category category);
     void deleteCategory(Long id);
     void updateCategory(Category category);
}
