package kz.iitu.categoryService.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Meal {
    private Long id;
    private String name;
    private Long categoryId;
    private String img;

}
