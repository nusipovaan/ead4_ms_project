package kz.iitu.ratingService.controller;

import kz.iitu.ratingService.model.Rating;
import kz.iitu.ratingService.service.RatingService;
import kz.iitu.ratingService.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;

@RestController
@RequestMapping("/rating")
@Api(value = "Rating controller", description = "Controller allows to get rating of recipes")
public class RatingController {

    private final Producer producer;
    @Autowired
    private RatingService ratingService;

    public RatingController(Producer producer) {
        this.producer = producer;
    }

    @ApiOperation(value = "To get all ratings from the database", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("")
    public ResponseEntity<?> getAllRatings() {
        return ResponseEntity.ok(ratingService.getAllRatings());
    }

    @ApiOperation(value = "To get the rating of the specific recipe", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/recipe/{id}")
    public ResponseEntity<?> getRatingOfRecipe(@PathVariable Long id){
        return ResponseEntity.ok(ratingService.getRecipeRating(id));
    }

    @ApiOperation(value = "To rate specific recipe", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PostMapping("")
    public ResponseEntity<?> addRating(@RequestBody Rating rating) {
        ratingService.addRating(rating);
        this.producer.ratingRequestNotify(rating);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "To remove rating by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRating(@PathVariable Long id) {
        ratingService.deleteRating(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @ApiOperation(value = "To update rating by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PutMapping("/{id}")
    public ResponseEntity<?> updateRating(@PathVariable Long id, @RequestBody Rating rating) {
        rating.setId(id);
        ratingService.updateRating(rating);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
