package kz.iitu.ratingService.service;

import kz.iitu.ratingService.model.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {
    private static final String TOPIC = "comment_requests";

    @Autowired
    private KafkaTemplate<String, Rating> kafkaTemplate;

    public String ratingRequestNotify(Rating rating) {
        System.out.println(String.format("#### -> Producing rating request to notification service -> %s", rating));
        this.kafkaTemplate.send(TOPIC, rating);
        return "Successfully";
    }
}
